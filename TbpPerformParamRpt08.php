<?php

/**
 * This is the model class for table "tbp_perform_param_rpt08".
 *
 * The followings are the available columns in table 'tbp_perform_param_rpt08':
 * @property string $id
 * @property string $empno
 * @property string $rpttype
 * @property string $rptname
 * @property string $check
 * @property string $sequence
 * @property string $opt1
 * @property string $opt2
 * @property string $opt3
 * @property string $cemp
 * @property string $uemp
 * @property string $ctime
 * @property string $utime
 * @property string $ip
 */
class TbpPerformParamRpt08 extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'tbp_perform_param_rpt08';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
//            array('cemp, ctime, ip', 'required'),
            array('empno, rpttype, rptname, cemp, uemp', 'length', 'max'=>8),
            // array('check, sequence', 'length', 'max'=>1000),
            array('opt1', 'length', 'max'=>1),
            array('opt2, opt3', 'length', 'max'=>20),
            array('ip', 'length', 'max'=>15),
            array('utime', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, empno, rpttype, rptname, check, sequence, opt1, opt2, opt3, cemp, uemp, ctime, utime, ip', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                // 取得員工資料
                'rname' => array(self::BELONGS_TO, 'TbpPerformParamRpt08Name', array('rptname'=>'id')),
                'rtype' => array(self::BELONGS_TO, 'TbpPerformParamRpt08Type', array('rpttype'=>'id')),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
                    return array(
                        'id' => 'id',
                        'empno' => '員編',
                        'rpttype' => '報表類型',
                        'rptname' => '報表名稱',
                        'check' => '選取項目',
                        'sequence' => '順序',
                        'opt1' => '是否使用',
                        'opt2' => '備用2',
                        'opt3' => '備用3',
                        'cemp' => '建立人員',
                        'uemp' => '修改人員',
                        'ctime' => '建立時間',
                        'utime' => '修改時間',
                        'ip' => '異動IP',
                    );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('t.id',$this->id,true);
            $criteria->compare('t.empno',$this->empno,true);
            $criteria->compare('rtype.rpttype',$this->rpttype,true);
            $criteria->compare('rname.rptname',$this->rptname,true);
            $criteria->compare('t.check',$this->check,true);
            $criteria->compare('t.sequence',$this->sequence,true);
            $criteria->compare('t.opt1',$this->opt1,true);
            $criteria->compare('t.opt2',$this->opt2,true);
            $criteria->compare('t.opt3',$this->opt3,true);
            $criteria->compare('t.cemp',$this->cemp,true);
            $criteria->compare('t.uemp',$this->uemp,true);
            $criteria->compare('t.ctime',$this->ctime,true);
            $criteria->compare('t.utime',$this->utime,true);
            $criteria->compare('t.ip',$this->ip,true);

            return new CActiveDataProvider($this, array(
                
                'criteria'=>$criteria,
                'sort' => array(
                    'defaultOrder' => 't.rpttype DESC',
                ),                
                
            ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TbpPerformParamRpt08 the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
  
    /**
     * 取得欄位
     * @return array
     */
    public function getRptCol(){

        $col = array();
        array_push($col, 'pdate');
        array_push($col, 'area');
        array_push($col, 'storename');
        array_push($col, 'empname');
        array_push($col, 'total');
        array_push($col, 'output');
        array_push($col, 'remit');
        array_push($col, 'realremit');
//        array_push($col, 'realtype');
        array_push($col, 'realmemo');
        array_push($col, 'col1'  );// 剪髮(全)
        array_push($col, 'col39' );// 剪髮100(全)
        array_push($col, 'col40' );// 剪髮110(全)

        array_push($col, 'col401' );// 剪髮110(折10)(全)
        array_push($col, 'col43' );// 剪80業100(全)

        array_push($col, 'col411' );// 剪髮120(全)

        array_push($col, 'col44' );// 女剪150(全)
        array_push($col, 'col441' );// 女剪130(全)
        array_push($col, 'col442' );// 女剪120(全)
        array_push($col, 'col443' );// 女剪130(折10)(全)
        array_push($col, 'col444' );// 女剪130(業134)(全)
        array_push($col, 'col445' );// 女剪180(全)

        // array_push($col, 'col451' );// 茶樹SPA120(全)
        // array_push($col, 'col452' );// 薄荷深層120(全)
        // array_push($col, 'col453' );// 舒活果漾120(全)
        // array_push($col, 'col454' );// 綠茶清新120(全)

        array_push($col, 'shamp100' );// 洗髮精<BR>100(全)
        array_push($col, 'shamp120' );// 洗髮精<BR>120(全)

        array_push($col, 'col36' );// 刻線(全)
        array_push($col, 'col37' );// 刻線100(全)
        array_push($col, 'col38' );// 刻線50(全)

        array_push($col, 'col2'  );// 染髮   
        array_push($col, 'col3'  );// 助染   
        array_push($col, 'col4'  );// 洗髮(全) 
        array_push($col, 'col5'  );// 優染   
        array_push($col, 'col6'  );// 優助染 
        array_push($col, 'col7'  );// 舒活SPA
        array_push($col, 'col8'  );// 養護SPA
        array_push($col, 'col9'  );// 洗髮精(全)

        array_push($col, 'col10' );// 剪髮(促)2人同行100
        array_push($col, 'col101');// 剪髮(促)2人同行110        
        array_push($col, 'col102');// 剪髮(促)2人同行120 (含女性)
        array_push($col, 'col103');// 剪髮(促)2人同行130 (含女性)


        array_push($col, 'col11' );// 洗髮(促)
        array_push($col, 'col12' );// 女性染髮
        array_push($col, 'col13' );// 女性助染
        array_push($col, 'col14' );// 男性染髮
        array_push($col, 'col15' );// 男性助染

        array_push($col, 'col41' );// 女無氨(全)
        array_push($col, 'col42' );// 男無氨(全)

        array_push($col, 'col16' );// 深層洗護
        array_push($col, 'col17' );// 深層洗護(購買深層護髮素)
        array_push($col, 'col18' );// 頭皮舒壓110 2016/9/1 修正
        array_push($col, 'col31' );// 頭皮舒壓120 2016/9/1 修正
        array_push($col, 'col28' );// 頭皮舒壓(加)100 2016/9/1 修正
        array_push($col, 'col32' );// 頭皮舒壓(全) 2016/9/1 修正

        array_push($col, 'col33' );// 頭皮舒壓150 2019/1/10 修正
        array_push($col, 'col34' );// 頭皮舒壓(加)130 2019/1/10 修正

        array_push($col, 'col35' );// 瞬間護髮100 2019/4/18 修正

        array_push($col, 'col19' );// 頭皮隔離
        array_push($col, 'col20' );// 髮油
        array_push($col, 'col21' );// 髮雕
        array_push($col, 'col22' );// 髮蠟
        array_push($col, 'col23' );// 髮凍
        array_push($col, 'col24' );// 頭皮保養液
        array_push($col, 'col25' );// 護色洗髮精
        array_push($col, 'col26' );// 深層護髮素
        array_push($col, 'col27' );// 洗髮(+10)
        array_push($col, 'col29' );// 洗髮(+20)
        array_push($col, 'col30' );// 洗髮(+30)
        
        array_push($col, 'perform');
        array_push($col, 'assist');

        array_push($col, 'col61' );// 精-剪髮(全)',

        array_push($col, 'col75' );// 精-刻線(全)',
        array_push($col, 'col76' );// 精-刻線200(全)',
        array_push($col, 'col77' );// 精-刻線100(全)',
        array_push($col, 'col78' );// 精-刻線50(全)',

        array_push($col, 'col62' );// 洗髮',       
        array_push($col, 'col63' );// 染(女)',     
        array_push($col, 'col64' );// 染助(女)',    
        array_push($col, 'col65' );// 染(男)',     
        array_push($col, 'col66' );// 助染(男)',    
        array_push($col, 'col67' );// 頭皮舒壓 2016/9/1 修正,
        array_push($col, 'col68' );// 舒壓(助) 2016/9/1 修正,
        array_push($col, 'col69' );// 隔離',       
        array_push($col, 'col70' );// 隔離(助)',    
        array_push($col, 'col71' );// 染剪護',      
        array_push($col, 'col72' );// 染剪護(助)',   
        array_push($col, 'col73' );// 染後護',      
        array_push($col, 'col74' );// 染後護(助)'    

        array_push($col, 'colS00' );// VIP免費券, 
        array_push($col, 'colS01' );// 10元折價券, 
        array_push($col, 'colS02' );// 20元折價券, 
        array_push($col, 'colS03' );// 30元折價券, 
        array_push($col, 'colS04' );// 40元折價券, 
        array_push($col, 'colS05' );// 50元折價券, 
        
        return $col;
    }

    /**
     * 取得標題
     * @return array
     */
    public function getRptTitle(){
        $title = array(
            'pdate'=>'日期',
            'area' => '營業區',
            'storename' => '門市',
            'empname' => '員工',
            'total' => '合計',
            'output' => '支出',
            'remit' => '匯款金額',
            'realremit' => '實際匯款金額',

//            'realtype' => '實際匯款類別',
            'realmemo' => '實際匯款備註',
            'col1' => '剪髮(全)',

            'col39' => '剪髮<BR>100<BR>(全)',
            'col40' => '剪髮<BR>110<BR>(全)',
            'col401' => '剪髮<BR>110<BR>(折10)(全)',
            // 2020/07/01 新增
            'col43' => '剪80<BR>業100<BR>(全)',
            // 2023/8/17 剪髮120
            'col411' => '剪髮<BR>120<BR>(全)',

            'col44' => '女剪<BR>150<BR>(全)',
            'col441' => '女剪<BR>130<BR>(全)',
            'col442' => '女剪<BR>120<BR>(全)',
            'col443' => '女剪130<BR>(折10)<BR>(全)',
            'col444' => '女剪130<BR>(業134)<BR>(全)',
            'col445' => '女剪<BR>180<BR>(全)',

            // 'col451' => '茶樹SPA<BR>120<BR>(全)',
            // 'col452' => '薄荷深層<BR>120<BR>(全)',
            // 'col453' => '舒活果漾<BR>120<BR>(全)',
            // 'col454' => '綠茶清新<BR>120<BR>(全)',

            'shamp100' => '洗髮精<BR>100(全)',
            'shamp120' => '洗髮精<BR>120(全)',

            // 2019/03/01 新增
            'col36' => '刻線(全)',
            'col37' => '刻線<BR>100<BR>(全)',
            'col38' => '刻線<BR>50<BR>(全)',

            'col2' => '染髮(全)',
            'col3' => '助染(全)',
            'col4' => '洗髮(全)',
            'col5' => '優染(全)',
            'col6' => '優助染(全)',
            'col7' => "舒活SPA<br>(全)",
            'col8' => "養護SPA<br>(全)",
            'col9' => '洗髮精(全)',
            'col10' => '剪髮(50)(全)',
            'col101' => '剪髮(55)(全)',
            'col102' => '剪髮(60)(全)',
            'col103' => '剪髮(65)(全)',

            'col11' => '洗髮(０)(全)',
            'col12' => '女性染髮(全)',
            'col13' => '女性助染(全)',
            'col14' => '男性染髮(全)',
            'col15' => '男性助染(全)',

            'col41' => '女無氨(全)',
            'col42' => '男無氨(全)',

            'col16' => '深層洗護(全)',
            'col17' => '深層洗護(購)(全)',
            // 舒壓 100 + 110 + 120 + 130 + 150
            'col18' => "頭皮舒壓<BR>110<BR>(全)",
            'col19' => '頭皮隔離(全)',
            'col20' => '髮油(全)',
            'col21' => '髮雕(全)',
            'col22' => '髮蠟(全)',
            'col23' => '髮凍(全)',
            'col24' => '頭皮保養液(全)',
            'col25' => '護色洗髮精(全)',
            'col26' => '深層護髮素(全)',
            'col27' => '洗髮(+10)(全)',
            // 頭皮舒壓
            'col31' => "頭皮舒壓<BR>120<BR>(全)",
            'col32' => '頭皮舒壓(全)',

            'col28' => '頭皮舒壓<BR>(加)<BR>100<BR>(全)',

            // 頭皮舒壓150 2019/1/10 修正
            'col33' => "頭皮舒壓<BR>150<BR>(全)",
            'col34' => '頭皮舒壓<BR>(加)<BR>130<BR>(全)',
            // 瞬間護髮100 2019/4/18 修正
            'col35' => '瞬間護髮<BR>100<BR>(全)',

            'col29' => '洗髮(+20)(全)',
            'col30' => '洗髮(+30)(全)',
            'perform' => '業績(全)',
            'assist' => '洗助染(全)',
            
            // 精剪達人
            'col61' => '精-剪髮(全)',
            
            // 2019/03/01 新增
            'col75' => '精-刻線(全)',
            'col76' => '精-刻線200(全)',
            'col77' => '精-刻線100(全)',
            'col78' => '精-刻線50(全)',

            'col81' => '精-菲德(全)',
            'col811' => '精-菲德350(全)',
            'col812' => '精-菲德300(全)',

            'col62' => '精-洗髮(全)',
            'col63' => '精-染(女)(全)',
            'col64' => '精-染助(女)(全)',
            'col65' => '精-染(男)(全)',
            'col66' => '精-助染(男)(全)',
            'col67' => '精-舒壓(全)(全)',
            'col68' => '精-舒壓(全)(助)(全)',
            'col69' => '精-隔離(全)',
            'col70' => '精-隔離(助)(全)',
            'col71' => '精-染剪護(全)',
            'col72' => '精-染剪護(助)(全)',
            'col73' => '精-染後護(全)',
            'col74' => '精-染後護(助)(全)',

            'colS00' => 'VIP免費券',
            'colS01' => '10元折價券',
            'colS02' => '20元折價券',
            'colS03' => '30元折價券',
            'colS04' => '40元折價券',
            'colS05' => '50元折價券'
            
        );

        return $title;
    }
}
