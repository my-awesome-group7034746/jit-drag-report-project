<?php
/* @var $this TbpPerformAuditController */
/* @var $dataProvider CActiveDataProvider */

//$this->breadcrumbs=array(
//	'Tbp Performs',
//);

//$this->menu=array(
//	array('label'=>'Create TbpPerform', 'url'=>array('create')),
//	array('label'=>'Manage TbpPerform', 'url'=>array('admin')),
//);
?>

<h1>設定報表顯示項目</h1><br>
<hr><br>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
?>
<div class="tableBlue">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tbp-perform-param-rpt08-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>

    <table>
      <tr>
      <td>
          <?php echo $form->labelEx($model,'empno'); ?>：
            <?php echo $form->textField($model,'empno',array('size'=>8,'maxlength'=>8)); ?>
            <?php echo $form->error($model,'empno'); ?>
      </td>
      <td>
      報表類型：<?php
                        echo $form->dropDownList($model,'rpttype', 
                                    CHtml::listData(
                                        TbpPerformParamRpt08Type::model()->findAll(array('order'=>'id ASC','condition'=>'opt1=1')
                                    ), 'id', 'rpttype'),
                                    array(
                                        'prompt'=>'選擇類別',
                                        'ajax' => array(
                                        'type'=>'POST', //request type
                                        'url'=>CController::createUrl('tbpPerformParamRpt08/dynamicstores'), //url to call.
                                        //Style: CController::createUrl('currentController/methodToCall')
                                        'update'=>'#TbpPerformParamRpt08_rptname', //selector to update
                                        //'data'=>'js:javascript statement' 
                                        //leave out the data key to pass all form values through
                        )));       
                    ?>
      </td>
      <td>
      報表名稱：<?php
                        echo $form->dropDownList($model,'rptname', CHtml::listData(
                                        TbpPerformParamRpt08Name::model()->findAll(
                                            array('order'=>'id ASC','condition'=>'opt1=1')),'id', 'rptname'),
                                            array('prompt'=>'選擇報表')
                                );
                        ?>
      </td>
      <td>
          <?php echo CHtml::submitButton('查詢',array('name'=>'qry')); ?>
      </td>
      </tr>
      <tr>
          <td>選取服務項目：</td>
          <td >
              <?php 
              echo $form->checkBoxList($model,'check',$serviceary);
              ?>
          </td>
          <td>服務項目順序</td>
          <td>選取服務項目：</td>
      </tr>
      <tr><td colspan="4"><?php echo CHtml::submitButton('送出',array('name'=>'save')); ?></td></tr>
     </table>
<?php $this->endWidget(); ?>
     

</div>

