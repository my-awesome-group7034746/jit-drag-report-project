<?php

?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.css" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-3.5.1.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.12.1/jquery-ui.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap5/bootstrap.css">
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap5.1.3/bootstrap.bundle.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vue/unpkg.com_vue@3.3.4_dist_vue.global.prod.js"></script>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/all.css">

<style>
    ul {
        list-style-type: none;
    }

    .first-color {
        background: #e0ffcd;
    }

    .second-color {
        background: #fdffcd;
    }

    .third-color {
        background: #ffebbb;
    }

    .fourth-color {
        background: #ffcab0;
    }

    .fifth-color {
        background: #d8b5de;
    }

    .sixth-color {
        background: #5a67a6;
    }

    .seventh-color {
        background: #fcb241;
    }

    .eight-color {
        background: #d68d08;
    }

    .clickable-span {
        cursor: pointer;
    }

    .can-use:hover {
        background: #ffebbb;
    }

    .already-use:hover {
        background: #e0ffcd;
    }

    .myHolder {
        background: #fdffcd;
        border: 3px solid black;
        border-radius: 5px;
        width: 5rem;
        height: 3rem;
    }
</style>

<div id="app">
    <!-- 顯示一些資訊e.g.後端傳到前端的結果暫時輸出的地方，或者告知使用者一些資訊的地方 -->
    <p id="information">{{post}}</p>
    <div v-if="show_info" id="redirect"></div>


    <!-- ajax的select input -->
    <div id="choose-action" class="fifth-color container mb-5 container pt-3" style="border-radius: 10px;">
        <div class="row">
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'tbp-perform-param-rpt08-form',
                'enableAjaxValidation' => false,
            )); ?>
            <div class="container search-bar mb-2">
                <div class="row">
                    <div class="col-5 fs-3">
                        <?php echo $form->labelEx($model, 'rpttype'); ?>
                        <?php
                        echo $form->dropDownList(
                            $model,
                            'rpttype',
                            CHtml::listData(
                                TbpPerformParamRpt08Type::model()->findAll(
                                    array('order' => 'id ASC', 'condition' => 'opt1=1')
                                ),
                                'id',
                                'rpttype'
                            ),
                            array(
                                'prompt' => '選擇類別',
                                'ajax' => array(
                                    'type' => 'POST', //request type
                                    'url' => CController::createUrl('tbpPerformParamRpt08/dynamicstores'), //url to call.
                                    //Style: CController::createUrl('currentController/methodToCall')
                                    'update' => '#TbpPerformParamRpt08_rptname', //selector to update
                                    //'data'=>'js:javascript statement' 
                                    //leave out the data key to pass all form values through
                                )
                            )
                        );
                        ?>
                        <?php echo $form->error($model, 'rpttype'); ?>
                    </div>
                    <div class="col-5 fs-3">
                        <?php echo $form->labelEx($model, 'rptname'); ?>
                        <?php
                        echo $form->dropDownList(
                            $model,
                            'rptname',
                            CHtml::listData(
                                TbpPerformParamRpt08Name::model()->findAll(
                                    array('order' => 'id ASC', 'condition' => 'opt1=1')
                                ),
                                'id',
                                'rptname'
                            ),
                            array('prompt' => '選擇報表')
                        );
                        ?>
                        <?php echo $form->error($model, 'rptname'); ?>
                    </div>
                    <div class="col-2"><input class="btn sixth-color fs-3 text-white search-btn mb-2" @click="searchTitle()" type="button" value="切換報表" name="search"></div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>


    <!-- 一個div包含左右兩邊，左邊是可使用的服務，右邊是目前報表有的服務 -->
    <div id="edit-window" class="d-flex w-100">
        <div id="left" class="container col-6 first-color pt-2 me-2" style="border-radius: 20px;">
            <div class="row">
                <div class="col-6">
                    <h2 style="font-weight: bold; display:inline">可使用服務</h2>
                </div>
                <div class="col-5">
                    <input class="ms-5" type="text" v-model="filterText" placeholder="輸入欲找尋項目" style="width: 16rem; height: 2.5rem;">
                </div>
            </div>
            <hr>
            <div class="row">
                <ul class="d-flex flex-wrap first-color p-2">
                    <li v-if="show_info" v-for="service in filteredServices" :id=service.col class="second-color m-2 p-1 clickable-span can-use fs-3" @click="addService(service)" style="border-radius: 15px;"><span class="fas fa-plus"></span>&nbsp;{{service.title}}</li>
                </ul>
            </div>
        </div>

        <div id="right" class="container col-6 third-color p-2" style="border-radius: 20px;">
            <div class="row">
                <div class="col-6">
                    <h2 style="font-weight: bold;">已使用服務</h2>
                </div>
                <div class="col-5">
                    <div class="row">
                        <input type="text" placeholder="輸入欲搜尋服務" v-model="searchText">
                        <button v-if="show_info" class="col-4 mt-1 btn btn-light" type="button" data-bs-toggle="modal" data-bs-target="#myModal">更新</button>
                    </div>

                </div>
            </div>
            <hr>
            <div class="row">
                <!-- 新增服務的window for hold tag -->
                <ul class="d-flex flex-wrap" id="add-item-window">
                    <li v-show="show_info" v-for="(usedService,index) in filterUsingServices" :id=usedService.col class="fourth-color m-2 p-1 clickable-span already-use fs-3" @click="removeService(usedService)" style="border-radius: 15px;"><span class="fas fa-minus"></span>
                        &nbsp;
                        <span class="fw-bold second-color number" style="display:inline-block">{{index}}</span>
                        {{usedService.title}}
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <!-- The modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content fifth-color">
                <!-- modal header -->
                <div class="modal-header">
                    <h3 style="color:#5a67a6;font-weight: bolder;" class="fs-1">⁉修改報表確認</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <button type="button" class="btn btn-lg fs-2" style="color:#fcb241; background:#e0ffcd; font-weight:bolder;" data-bs-dismiss="modal" id="confirm" @click="updateDrag()">確認</button>
                    <button class="btn btn-lg fs-2 seventh-color text-dagner ms-1" type="button" data-bs-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>

    <!-- uses solid style -->
</div>

<script>
    //把我們的vue暫存的變數，等等全域的jquery操作會用到
    var gl_vm;

    //vue實例
    Vue.createApp({

        //變數使用
        data() {
            return {
                available: [], //所有可以使用的服務
                using: [], //如果有我們對應的報表,他目前有的服務
                filterText: '', //綁定輸入的值，用來過濾可以使用的服務
                searchText: '', //已使用關鍵字，用來過濾以使用的服務
                post: "", //先看我們的post
                show_info: false,
                tmp_rpt_name: '' //目前的報表名稱
                //rpttype: "",
                //rptname: "",
            }
        },

        //方法使用
        methods: {

            searchTitle() {
                /**
                 * 利用droplist到後端搜尋資料然後更新using
                 */
                //防止form的重整
                event.preventDefault();
                //選擇type的dropList
                var type = document.getElementById("TbpPerformParamRpt08_rpttype");
                //獲取type的value
                var type_value = type.value;
                var type_text = type.options[type.selectedIndex].text;
                //console.log("tpye的value:", type_value);
                //console.log("type的文字:", type_text);

                var name = document.getElementById("TbpPerformParamRpt08_rptname");
                var name_value = name.value;
                //var name_text = name.options[name.selectedIndex].text;
                //console.log("name的value:", name_value);
                //console.log("name的文字:", name_text);

                //條件語句，檢查下拉選單的值是否為空
                if (name_value == "" || name.options[name.selectedIndex].text === undefined) {
                    //若為空，在上方告知使用者的資訊給指示
                    this.post = "請選擇一個報表做更新";
                    $("#information").removeClass();
                    $("#information").addClass("text-danger fs-1 fw-bold");
                } else {
                    var vm = this //保存vue的this物件
                    $.ajax({
                        url: "<?php echo Yii::app()->request->baseUrl ?>/index.php/tbpPerformParamRpt08/getUsing",
                        method: "POST",
                        dataType: "json",
                        data: {
                            type: type_value,
                            name: name_value,
                        },
                        success: function(data) {
                            //console.log(data)
                            if (data.length >= 1 && !data[0].title) {
                                vm.post = "目前的報表不在tbp_perform_param_rpt08無法更新排序"
                                vm.show_info = false;
                                $("#information").removeClass();
                                $('#information').addClass("text-success fw-bold fs-1");
                            } else {
                                vm.using = data;
                                vm.show_info = true;
                                vm.post = "";
                            }


                            //console.log(vm.post);
                        },
                        error: function(data, status, err) {
                            vm.post = data.status + " " + data.statusText + " " + 'TbpPerformParamRpt08Controller/actionGetUsing有錯誤請截圖傳給資訊組';
                            $("#information").removeClass();
                            $("#information").addClass("text-danger fw-bold fs-2");
                        }
                    })
                }


            },
            //利用table來判斷是否存在
            //本來想使用include但是，proxy object會和object認為不同的物件
            //導致會新增重複
            //製作using的table
            //table若存在相同名稱回傳true
            //else回傳false
            tableCheck(service, title) {
                table = {};
                for (i = 0; i < service.length; i++) {
                    table[service[i].title] = true;
                }
                if (table[title]) {
                    return true;
                } else {
                    return false;
                }
            },

            addService(service) {
                //console.log(this.using)
                if (this.tableCheck(this.using, service.title)) {
                    alert("服務已使用，請勿重複");
                } else {
                    //新增item也要順便新增其idx
                    this.using.push(service);
                }
            },
            removeService(usedService) {
                //when we click the li tag, we get the usedService index item {col: xxx, title: xxx}
                //use filter return the array that not include the usedService title
                this.using = this.using.filter(service => !service.title.includes(usedService.title));
                //刪除以後需要重新排序我們的服務項目順序
                //console.log(this.using);
            },
            updateDrag() {
                var type = document.getElementById("TbpPerformParamRpt08_rpttype");
                //獲取type的value
                var type_value = type.value;
                var type_text = type.options[type.selectedIndex].text;
                //console.log("tpye的value:", type_value);
                //console.log("type的文字:", type_text);
                var name = document.getElementById("TbpPerformParamRpt08_rptname");
                var name_value = name.value;
                var name_text = name.options[name.selectedIndex].text;
                //console.log("name的value:", name_value);
                //console.log("name的文字:", name_text);
                //根據拖動在更新我們的服務
                var re_using = this.reconstructUsing();
                this.post = "";
                var vm = this;
                //必須記錄uemp是誰才能知道上一個動過修改報表的是誰
                var empno = <?php echo $empno; ?>;
                $.ajax({
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/tbpPerformParamRpt08/updateDrag",
                    method: "POST",
                    dataType: "text",
                    data: {
                        type: type_value,
                        name: name_value,
                        checked: re_using,
                        empno: empno,
                    },
                    success: function(data) {
                        vm.post = data;
                        $("#information").removeClass();
                        $("#information").addClass("text-success fs-1 fw-bold");
                        $("#redirect").html("<a href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/tbpPerformRpt/rpt08' target='_blank'>管理部報表</a>")
                    },
                    error(data, status, error) {
                        vm.post = data.status + " " + data.statusText + " " + "tbpPerfomrParamRpt08Controller/updateDrag有問題請截圖給資訊組";
                        $("#information").removeClass();
                        $("#information").addClass("text-danger fs-1 fw-bold");
                    }
                });
            },
            reconstructUsing() {
                const using = [];
                document.querySelectorAll("#add-item-window li").forEach(li => {
                    const col = li.getAttribute('id');
                    using.push(col);
                });
                //console.log(using);
                return using;
            }

        },
        //基本上不需要()所以可以當成一個變數使用
        computed: {
            filteredServices: function() {
                //根據輸入框的值來過濾服務列表
                const filterText = this.filterText;
                //過濾我們的available array 每個 index 叫做service 其中每個index的object會有title 和我們輸入的有關的才會更新
                return this.available.filter(service => service.title.includes(filterText));
            },
            filterUsingServices: function() {
                //根據關鍵字過濾服務列表
                const searchText = this.searchText;
                return this.using.filter(service => service.title.includes(searchText));
            },
        },
        mounted() {
            var vm = this //保存vue的this物件
            gl_vm = this;
            console.log("I am mounted");
            $.ajax({
                url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/tbpPerformParamRpt08/getServices",
                dataType: "json",
                success: function(data) {
                    //因為此處沒有辦法使用this這邊的this是指向windows全局對象
                    vm.available = data;
                }
            });
        },

    }).mount('#app');

    function initSortable() {
        $("#add-item-window").sortable({
            update: function(event, ui) {
                //拖動的時候更新idx
                //ui.item代表的是被拖動的元素在這裡就是li
                //所以ui.item.parent()就是他的父元素，在這裡就是ul
                //再加上children代表其所有子元素
                var sortedItems = ui.item.parent().children();
                
                //遍歷每個子元素，並更新其中的數字
                sortedItems.each(function(index){
                    //用法是父元素，尋找class為number的子元素
                    $(this).find('.number').text(index);
                });
            }
        });
    }
    initSortable();

    $("#TbpPerformParamRpt08_rpttype").change(function() {
        gl_vm.show_info = false;
    });

    $('#TbpPerformParamRpt08_rptname').change(function() {
        gl_vm.show_info = false;
    });
</script>