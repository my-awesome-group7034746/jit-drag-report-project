<?php
/* @var $this TbpPerformParamRpt08Controller */
/* @var $model TbpPerformParamRpt08 */

//$this->breadcrumbs=array(
//	'Tbp Perform Param Rpt08s'=>array('index'),
//	$model->id,
//);

$this->menu=array(
//	array('label'=>'List TbpPerformParamRpt08', 'url'=>array('index')),
	array('label'=>'設定報表', 'url'=>array('create')),
	array('label'=>'修改報表', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'刪除報表', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'管理報表', 'url'=>array('admin')),
);
?>

<h1>檢視報表 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'empno',
		'rpttype',
		'rptname',
		'check',
                                      'sequence',
		'opt1',
		'opt2',
		'opt3',
		'cemp',
		'uemp',
		'ctime',
		'utime',
		'ip',
	),
)); ?>
