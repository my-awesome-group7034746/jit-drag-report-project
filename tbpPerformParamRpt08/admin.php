<?php
/* @var $this TbpPerformParamRpt08Controller */
/* @var $model TbpPerformParamRpt08 */
//
//$this->breadcrumbs=array(
//	'Tbp Perform Param Rpt08s'=>array('index'),
//	'Manage',
//);

$this->menu=array(
//	array('label'=>'List TbpPerformParamRpt08', 'url'=>array('index')),
	array('label'=>'新增報表權限', 'url'=>array('setrpt08')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tbp-perform-param-rpt08-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>管理報表權限</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tbp-perform-param-rpt08-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
                    'name' => 'rpttype',
                    'value' => 'isset($data->rtype)?$data->rtype->rpttype:""',
                ),	
                array(
                    'name' => 'rptname',
                    'value' => 'isset($data->rptname)?$data->rname->rptname:""',
                ),
                'empno',
//		'check',
//                                      'sequence',
		/*
		'opt1',
		'opt2',
		'opt3',
		'cemp',
		'uemp',
		'ctime',
		'utime',
		'ip',
		*/
		array(
			'class'=>'CButtonColumn',
			'template' => '{view} {update} {delete} {reorder}',
			'buttons' => array(
				'reorder' => array(
					'label' => '排序',
					'url' => 'Yii::app()->controller->createUrl("update2",array("id"=>$data->primaryKey))',
					'imageUrl' => Yii::app()->request->baseUrl . '/images/icon/reorder.png'
				)
			)
		),
		
	),
)); 
?>

