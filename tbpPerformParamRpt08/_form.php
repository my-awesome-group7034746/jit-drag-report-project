<?php
/* @var $this TbpPerformParamRpt08Controller */
/* @var $model TbpPerformParamRpt08 */
/* @var $form CActiveForm */
?>
<!-- sweetAlert -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- jquery sortable -->
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<!-- selectize.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.6/js/selectize.min.js" integrity="sha512-DBOconMAY06o4R79zeXKKM3h/g5pca647Eabb+6viK4dRpiMOlZFS4gsbukTbHo+ppdKx4yr+/0m2JnpeAIrSw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.6/js/standalone/selectize.min.js" integrity="sha512-pgmLgtHvorzxpKra2mmibwH/RDAVMlOuqU98ZjnyZrOZxgAR8hwL8A02hQFWEK25V40/9yPYb/Zc+kyWMplgaA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.6/css/selectize.css" integrity="sha512-6skR4yyaANUKXypVS+nB+HMmq8Xd17CSwFsBEHCRaa3UicPlksbwVtBTZl13Fea6zqsnnmqc7fRH97/M6JcwCA==" crossorigin="anonymous" referrerpolicy="no-referrer" />


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tbp-perform-param-rpt08-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><span class="required">*</span>為必填.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'empno'); ?>
		<?php echo $form->textField($model,'empno',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'empno'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rpttype'); ?>
		<?php
                                            echo $form->dropDownList($model,'rpttype', 
                                                        CHtml::listData(
                                                            TbpPerformParamRpt08Type::model()->findAll(array('order'=>'id ASC','condition'=>'opt1=1')
                                                        ), 'id', 'rpttype'),
                                                        array(
                                                            'prompt'=>'選擇類別',
                                                            'ajax' => array(
                                                            'type'=>'POST', //request type
                                                            'url'=>CController::createUrl('tbpPerformParamRpt08/dynamicstores'), //url to call.
                                                            //Style: CController::createUrl('currentController/methodToCall')
                                                            'update'=>'#TbpPerformParamRpt08_rptname', //selector to update
                                                            //'data'=>'js:javascript statement' 
                                                            //leave out the data key to pass all form values through
                                            )));       
                                        ?>
		<?php echo $form->error($model,'rpttype'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rptname'); ?>
		<?php
                        echo $form->dropDownList($model,'rptname', CHtml::listData(
                                        TbpPerformParamRpt08Name::model()->findAll(
                                            array('order'=>'id ASC','condition'=>'opt1=1')),'id', 'rptname'),
                                            array('prompt'=>'選擇報表')
                                );
                        ?>
		<?php echo $form->error($model,'rptname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'check'); ?>
		<?php echo $form->textArea($model,'check',array('style'=>'width: 100%; height: 50px;')); ?>
		<?php echo $form->error($model,'check'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sequence'); ?>
		<?php echo $form->textField($model,'sequence',array('style'=>'width: 100%; height: 50px;')); ?>
		<?php echo $form->error($model,'sequence'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'opt1'); ?>
		<?php echo $form->radioButtonList( $model,'opt1', array( '1' => '是', '0' => '否' ), array('separator' =>' ') );        ?>
		<?php echo $form->error($model,'opt1'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '新增' : '儲存'); ?>
	</div>
        <br>
<?php $this->endWidget(); ?>
</div><!-- form -->
<h2>系統欄位</h2>
<div class = 'tableBlue'>
<?php
    
    $ary = TbpPerformParamRpt08::model()->getRptTitle();
    echo '<table>';
		echo '<tr><td>欄位</td><td>中文</td></tr>';
		$n = 0;
		foreach ($ary as $key => $value) {
			$n++;
			echo '<tr>';
				echo '<td>'.$key.'</td>';
				echo '<td>'.$value.'</td>';
			echo '</tr>';
		}
    echo '</table>';
?>
</div>
<!-- 系統操作 -->

<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"  data-backdrop="false">新增刪減、排序修正</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">新增刪減、排序修正</h4>
      </div>
      <div class="modal-body">
		<!--  -->
		<div class="tab-content">
			<select name = 'rptItem' id = 'rptItem' multiple="multiple" >
				<?php 
					$selectedList = explode(",",$model['check']); 
					foreach($selectedList as $c)
					{
						echo "<option value='$c'  selected>$c</option>";
					}
				?>
			</select>
		</div>
      </div>
      <div class="modal-footer">
	  <button type = 'button' class = 'btn btn-primary' onclick="newCheckItemList()">確認修改</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
$( function() {
	$("#rptItem").selectize({
		plugins: ["drag_drop"],
		delimiter: ",",
		persist: false,
		create: function (input) {
			return {
			value: input,
			text: input,
			};
		},
	});
});
//更新rpt報表
function newCheckItemList()
{
	// LOADING
	Swal.fire({
                title:'更新中...',
                allowOutsideClick: false,
                didOpen:()=>{
                    Swal.showLoading()
                }
            });
	var rptName = document.getElementById('TbpPerformParamRpt08_rptname').value;
	var newCheckItems = $("#rptItem").selectize()
	var newCheckItemsAry = newCheckItems[0].selectize.lastValidValue;
	console.log(rptName);
	console.log(newCheckItemsAry);
	$.ajax({
			url:"<?php echo Yii::app()->createUrl('TbpPerformParamRpt08/AjaxChangeRptCheckItems'); ?>",
				type:'POST',
				dataType:'JSON',
				data:{
					newCheckItemsAry:newCheckItemsAry,
					rptName:rptName,
					//($post[name]) :var名稱
					//php後端呼叫方式 $_POST['itme']
				},
				success:function(data){
					console.log(data);
					if(data == '1')
					{
						Swal.fire(
                                '更新成功',
                                '',
                                'success'
                            );
					}
					else
					{
						Swal.fire(
                                '更新失敗',
                                '',
                                'error'
                            );
					}
				},
				error:function(){alert("ajax失敗");}
			});

}
  </script>