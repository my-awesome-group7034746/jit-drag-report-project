<?php
/* @var $this TbpPerformParamRpt08Controller */
/* @var $model TbpPerformParamRpt08 */
//
//$this->breadcrumbs=array(
//	'Tbp Perform Param Rpt08s'=>array('index'),
//	$model->id=>array('view','id'=>$model->id),
//	'Update',
//);

// $this->menu = array(
//     //	array('label'=>'List TbpPerformParamRpt08', 'url'=>array('index')),
//     array('label' => '新增報表權限', 'url' => array('create')),
//     //	array('label'=>'View TbpPerformParamRpt08', 'url'=>array('view', 'id'=>$model->id)),
//     array('label' => '管理報表權限', 'url' => array('admin')),
// );
?>

<h1>修改報表<?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form2', array(
                            'model' => $model, 
                            'reportCol' => $reportCol, 
                            'reportTitle' => $reportTitle,
                            'reportName' => $reportName,
                            'reportType' => $reportType)); ?>