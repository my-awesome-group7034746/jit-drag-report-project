<?php

?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.css" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-3.5.1.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.12.1/jquery-ui.js"></script>

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap5/bootstrap.css">
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap5.1.3/bootstrap.bundle.min.js"></script>
<style>
    /* 拖動sortable時,placeholder的格式 */
    .myHolder {
        background: silver;
        border: 3px solid black;
        border-radius: 5px;
        width: 10rem;
        height: 10rem;
    }

    /* 拖動會有卡頓嘗試看看的css */
    .d-flex {
        overflow: visible !important;
    }

    .flex-container {
        overflow: auto;
    }

    /* .row {
        transition: none;
    } */

    /* #sortable-1 .block {
        
    } */
</style>

<h1 class="fs-1">reorder title</h1>

<!-- yii activeform 實現ajax的改動 -->
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'tbp-perform-param-rpt08-form',
    'enableAjaxValidation' => false,
)); ?>
<div class="container search-bar mb-2">
    <div class="row">
        <div class="col-5 fs-3">
            <?php echo $form->labelEx($model, 'rpttype'); ?>
            <?php
            echo $form->dropDownList(
                $model,
                'rpttype',
                CHtml::listData(
                    TbpPerformParamRpt08Type::model()->findAll(
                        array('order' => 'id ASC', 'condition' => 'opt1=1')
                    ),
                    'id',
                    'rpttype'
                ),
                array(
                    'prompt' => '選擇類別',
                    'ajax' => array(
                        'type' => 'POST', //request type
                        'url' => CController::createUrl('tbpPerformParamRpt08/dynamicstores'), //url to call.
                        //Style: CController::createUrl('currentController/methodToCall')
                        'update' => '#TbpPerformParamRpt08_rptname', //selector to update
                        //'data'=>'js:javascript statement' 
                        //leave out the data key to pass all form values through
                    )
                )
            );
            ?>
            <?php echo $form->error($model, 'rpttype'); ?>
        </div>
        <div class="col-5 fs-3">
            <?php echo $form->labelEx($model, 'rptname'); ?>
            <?php
            echo $form->dropDownList(
                $model,
                'rptname',
                CHtml::listData(
                    TbpPerformParamRpt08Name::model()->findAll(
                        array('order' => 'id ASC', 'condition' => 'opt1=1')
                    ),
                    'id',
                    'rptname'
                ),
                array('prompt' => '選擇報表')
            );
            ?>
            <?php echo $form->error($model, 'rptname'); ?>
        </div>
        <div class="col-2"><input class="btn btn-lg btn-info fs-3" type="submit" value="查詢" name="search"></div>
    </div>

</div>

<div id="mydiv">

</div>
<!-- 從後端傳過來的show, true顯示以下的block for sortable拖動 -->
<?php if ($show == true) : ?>
    <div id="table" class="d-flex flex-column flex-container">
        <div id="report-info" class="d-flex">
            <div class="col-6 fs-3 bg-success text-center text-white">報表類型: <?php echo $reportType; ?></div>
            <div class="col-6 fs-3 bg-secondary text-center text-white">報表名稱: <?php echo $reportName; ?></div>
        </div>
        <div id="sortable-1" class="container" style="list-style-type: none;">
            <div class="row">
                <?php for ($i = 0; $i < count($reportTitle); $i++) : ?>
                    <div class="bg-warning fs-4 col-3 block m-2" style="height: 12rem; width:12rem; position:relative; ">
                        <span id="<?php echo $reportCol[$i]; ?>" class="item text-primary" style="position:absolute;left:30%; top:27%; font-weight:bold;"><?php echo $reportTitle[$i] . " "; ?></span>
                        <span class="num fs-1 bg-success text-light" style="font-weight:bolder; display:inline-block"><?php echo $i; ?></span>
                    </div>
                <?php endfor; ?>
            </div>

        </div>
    </div>

    <div class="d-flex flex-row-reverse mt-3" style="width: 80%;">
        <button id="modal-btn" type="button" class="btn btn-lg btn-primary" data-bs-toggle="modal" data-bs-target="#myModal">
            更改順序
        </button>
    </div>

    <!-- The model -->
    <div id="myModal" class="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal header -->
                <div class="modal-header">
                    <h1 class="modal-title">更新排序</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <!-- modal body -->
                <div class="modal-body fs-2">
                    🤨是否更新排序❓
                </div>

                <!-- modal footer -->
                <div class="modal-footer">
                    <button id="confirm" type="button" class="btn btn-success" data-bs-dismiss="modal">確定</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>



<script>
    function checkBoundary(pos) {
        if (pos.left < 0) {
            pos.left = 0;
        }
    }
    //把sortable做成一個function,因為在ajax complete之後我們會強制reload，屆時會把綁定的sortable事件給摧毀，
    //所以我們必須在complete的階段重新call initSortable function
    function initSortable() {
        $("#sortable-1").sortable({
            items: '.block',
            tolerance: "pointer",
            cursor: 'move',
            placeholder: 'myHolder',
            scroll: true,
            //placeholder: 'gray',
            update: function(event, ui) {

                ui.placeholder.css({
                    left: ui.position.left - 2,
                    right: ui.position.right - 2,
                });
                //獲取所有的行
                var rows = $('#sortable-1 div div');
                //console.log(rows);
                //遍歷設置 opt2
                rows.each(function(index) {
                    //獲取目前的div上面的文字
                    //有兩個span一個是商品名稱
                    //一個是順序
                    var cur_seq = $(this).find('.num').text();
                    //console.log(cur_str);
                    //將其轉變成陣列，因為我們有用空格把名稱和數字給隔開，所以我們用空格split我們的字串
                    //var cur_str_ary = cur_str.split(" ");
                    //console.log(cur_str_ary);
                    //自己寫的function利用目前的index更新順序
                    //cur_str = arrCombineStr(cur_str_ary, index);
                    //把更新好的字串設定在span裡面
                    $(this).find('.num').text(index);
                });
            },
            opacity: 0.6,
        });
    };

    function arrCombineStr(arr, index) {
        let filtered = arr.filter(s => s.trim() !== '');
        //將號碼改成我們的index
        filtered[1] = index;
        let str = filtered.join(' ');
        return str;
    }
    initSortable();
    $("#confirm").click(function() {
        const col_ary = [];
        const seq_ary = [];
        //get each id of my span, since each span id got the exact col or serviceno
        $("#sortable-1 .block").each(function(index) {
            const id = $(this).find("span").attr('id');
            col_ary.push(id);
            seq_ary.push(index);
        });

        console.log("this is my col:", col_ary, '\n');
        console.log("this is their sequence", seq_ary);
        //after get the sequece post it to controller to deal with the post
        let result = $.ajax({
            url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/tbpPerformParamRpt08/reorder",
            type: 'POST',
            data: {
                col: col_ary,
                seq: seq_ary,
                id: "<?php echo $id; ?>"
            },
            beforeSend: function() {



            },
            success: function(mydata) {
                $("#mydiv").html("<img src='<?php echo Yii::app()->request->baseUrl; ?>/images/Bean Eater-1s-200px.gif'>");
                setTimeout(function() {
                    $("#mydiv").html(mydata);
                }, 2000)

            }
        });

        //alert("更改成功，請到管理部報表做檢查");
        $('#myModal').modal('hide');
    });
</script>



<?php $this->endWidget(); ?>