<?php
/* @var $this TbpPerformParamRpt08Controller */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tbp Perform Param Rpt08s',
);

$this->menu=array(
	array('label'=>'Create TbpPerformParamRpt08', 'url'=>array('create')),
	array('label'=>'Manage TbpPerformParamRpt08', 'url'=>array('admin')),
);
?>

<h1>Tbp Perform Param Rpt08s</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
